<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'burgos_store' );

/** MySQL database username */
define( 'DB_USER', 'burgos_store' );

/** MySQL database password */
define( 'DB_PASSWORD', 'iXA!5O#5z(;N' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'apeGSTm/Ed$2HWsyD^.bEZs%@D-jmg L6g*[$#s>!n7J]rp9 +N9zd.= EyS+zvn' );
define( 'SECURE_AUTH_KEY',  'eAJT*/#kckf}G-bEN,6p945.Ed9=ioV,z77ygdz0b+k_o;P2ZXlaBUCM*fiE:tuD' );
define( 'LOGGED_IN_KEY',    'z[PI+*tKU=CD!x_0LV>{^o^unR9L.AVc=E1UI[h@d^WgyHUxd-zfwA*yCJ4p+P*T' );
define( 'NONCE_KEY',        'n;`?pMF}ma._2`y$F`|jNa&LCwL7>%C=A_Q%VMT#,Ns|0|4^L,P[.EK`0ga-C5`R' );
define( 'AUTH_SALT',        '4zGfSp%dOqmQ9pIFA6^Z$~0k5r(<`BTd&uo%9h+kWg*F7n!0mu)!Iz}VUeBl|WAr' );
define( 'SECURE_AUTH_SALT', 'l8&*LK#|ZG5ztSTUcO0R4-+HER7l>>2PdierC;>MK2er-t#`tn<J]_6ePA7}-=}n' );
define( 'LOGGED_IN_SALT',   'T62==!XkhH:ox?J|m:*5Vq},qp8j!v0y6>qRg/[cgj2eM-9]&`qMLJ^91.~MrQjy' );
define( 'NONCE_SALT',       'RsZ49oz+%}=]lvCMK76tNm`rBDgb(14PzO?7]0b,gviu{QUa{]bb$z1B6SjZ}9Cl' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
