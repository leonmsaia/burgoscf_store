<?php

// Configuration of WooCommerce
function burgoscfstore_add_woocommerce_support() {
    add_theme_support('woocommerce', array(
        'thumbnail_image_width' => 150,
        'single_image_width'    => 300,

        'product_grid'          => array(
            'default_rows'    => 3,
            'min_rows'        => 2,
            'max_rows'        => 8,
            'default_columns' => 4,
            'min_columns'     => 2,
            'max_columns'     => 5,
        ),
    ) );
}
add_action('after_setup_theme', 'burgoscfstore_add_woocommerce_support');

// Post Type Banner
function banner_post_type() {
    $labels = array(
        'name' => __('Banners'),
        'singular_name' => __('banner'),
        'add_new' => __('Nuevo banner'),
        'add_new_item' => __('Agregar Nuevo banner'),
        'edit_item' => __('Edit banner'),
        'new_item' => __('Nuevo banner'),
        'view_item' => __('Ver banner'),
        'search_items' => __('Buscar banners'),
        'not_found' =>  __('No se encontraron banners'),
        'not_found_in_trash' => __('No hay Banners en la Papelera'),
    );
    $args = array(
        'labels' => $labels,
        'has_archive' => true,
        'public' => true,
        'hierarchical' => false,
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'custom-fields',
            'thumbnail',
            'page-attributes'
        ),
        'taxonomies' => array('post_tag', 'category'),
    );
    register_post_type('banner', $args );
}
add_action('init', 'banner_post_type');
add_filter('show_admin_bar', '__return_false');

// Post Type Banner Mobile
function banner_mobile_post_type() {
    $labels = array(
        'name' => __('Banners Mobile'),
        'singular_name' => __('banner_mobile'),
        'add_new' => __('Nuevo banner mobile'),
        'add_new_item' => __('Agregar Nuevo banner mobile'),
        'edit_item' => __('Edit banner mobile'),
        'new_item' => __('Nuevo banner mobile'),
        'view_item' => __('Ver banner mobile'),
        'search_items' => __('Buscar banners mobile'),
        'not_found' =>  __('No se encontraron banners mobile'),
        'not_found_in_trash' => __('No hay Banners mobile en la Papelera'),
    );
    $args = array(
        'labels' => $labels,
        'has_archive' => true,
        'public' => true,
        'hierarchical' => false,
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'custom-fields',
            'thumbnail',
            'page-attributes'
        ),
        'taxonomies' => array('post_tag', 'category'),
    );
    register_post_type('bannermobile', $args );
}
add_action('init', 'banner_mobile_post_type');
add_filter('show_admin_bar', '__return_false');


// Site Forms
// Registration Form
function bbloomer_separate_registration_form() {
    if (is_admin()) return;
    if (is_user_logged_in()) return;
    ob_start();
    do_action('woocommerce_before_customer_login_form');

    get_template_part('sections/forms/registration');


   return ob_get_clean();
}
add_shortcode('wc_reg_form_bbloomer', 'bbloomer_separate_registration_form');

// Login Form
function bbloomer_separate_login_form() {
   if (is_admin()) return;
   if (is_user_logged_in()) return;
   ob_start();

   get_template_part('sections/forms/login');
   woocommerce_login_form( array('redirect' => get_site_url() . '/mi-cuenta'));
   
   return ob_get_clean();
}
add_shortcode('wc_login_form_bbloomer', 'bbloomer_separate_login_form');
  













?>