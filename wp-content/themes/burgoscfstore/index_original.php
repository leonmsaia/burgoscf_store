<!DOCTYPE html>
<html>
<head>
	<?php get_template_part('sections/assets/head');?>
</head>
<body>
	<div class="container-fluid top-navbar-wrapper">
		<span class="top-navbar-border-white"></span>
		<span class="top-navbar-border-black"></span>
	</div>
	<?php get_template_part('sections/modules/user-navbar');?>
	<?php get_template_part('sections/modules/header-complex');?>
	<?php get_template_part('sections/modules/hero-slider');?>
	<div class="container-fluid main-body-container home-wrapper">
		<div class="container">
			<div class="row">
				
				<div class="col-md-12">
					<?php // Exclusivas Module ?>
					<div class="row module-separator first-cta-home">
						<div class="col-md-12">
							<h1 class="prod-list-name">Exclusivas On-Line</h1>
						</div>
						<?php  
						    $args = array(
						        'post_type'      => 'product',
						        'posts_per_page' => 10
						    );
						    $loop = new WP_Query($args);
						?>
						<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
							<?php $real_val = wc_price($product->get_price()); ?>
							<?php $discounted_val = wc_price($product->get_regular_price()); ?>
								<div class="col-lg-4 col-sm-6">
									<div class="row note-display-wrapper product-display">
										<div class="col-md-12 note-display-category-wrapper">
											<h3 class="note-display-category">
												<?php echo wc_get_product_category_list($product->get_id());?>
											</h3>
										</div>
											<a class="col-md-12 note-display-image-wrapper" href="<?php echo get_site_url() . '/' . $product->get_slug(); ?>">
												<?php if ($discounted_val != $real_val AND $product->is_type('variable') == FALSE): ?>
													<span class="sale-label">Oferta</span>
												<?php endif ?>
												<?php echo woocommerce_get_product_thumbnail('woocommerce_single ');?>
											</a>
										<a class="col-md-12 note-display-title-wrapper" href="<?php echo get_site_url() . '/' . $product->get_slug(); ?>">
											<h1 class="note-display-title"><?php echo get_the_title();?></h1>
											<?php if ($product->is_type('variable') == TRUE): ?>
												<span class="sale-price">
													<?php echo 'Desde ' . wc_price($product->get_price());?>
												</span>
											<?php else: ?>
												<span class="sale-price">
													<?php echo wc_price($product->get_price());?>
												</span>
											<?php endif ?>
											<?php if ($discounted_val != $real_val AND $product->is_type('variable') == FALSE): ?>
												<span class="separator-price">-</span> 
												<span class="discounted-price">
													<?php echo wc_price($product->get_regular_price());?>
												</span>
											<?php endif ?>
										</a>
									</div>
								</div>
						<?php endwhile; ?>
						<?php wp_reset_query(); ?>
					</div>
					<?php // Exclusivas Module End ?>

					<?php // Banner Seccion 1 ?>
					<?php
						$query = new WP_Query( array(
					        'post_type' => 'banner',
					        'category_name' => 'home-seccion-1'
					    ));
					?>
					<?php while ( $query->have_posts() ) : $query->the_post(); ?>
						<div class="row module-separator banner-common">
							<div class="col-md-12 banner-container">
								<?php the_content(); ?>
							</div>
						</div>
					<?php endwhile; ?>
					<?php wp_reset_query(); ?>
					<?php // Banner Seccion 1 End ?>

					<?php // Banner Mobile Seccion 1 ?>
					<?php
						$query = new WP_Query( array(
					        'post_type' => 'bannermobile',
					        'category_name' => 'home-seccion-1'
					    ));
					?>
					<?php while ($query->have_posts()) : $query->the_post();?>
						<div class="row module-separator mobile_banner">
							<div class="col-md-12 banner-container">
								<?php the_content(); ?>
							</div>
						</div>
					<?php endwhile; ?>
					<?php wp_reset_query(); ?>
					<?php // Banner Mobile Seccion 1 End ?>
					
					<?php // Productos Destacados Module ?>
					<div class="row module-separator">
						<div class="col-md-12">
							<h1 class="prod-list-name">Productos Destacados</h1>
						</div>
						<?php  
							$tax_query   = WC()->query->get_tax_query();
							$tax_query[] = array(
							    'taxonomy' => 'product_visibility',
							    'field'    => 'name',
							    'terms'    => 'featured',
							    'operator' => 'IN',
							);
						    $args = array(
						        'post_type'      => 'product',
						        'posts_per_page' => 4,
						        'tax_query'  =>  $tax_query
						    );
						    $loop = new WP_Query($args);
						?>
						<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
							<?php $real_val = wc_price($product->get_price()); ?>
							<?php $discounted_val = wc_price($product->get_regular_price()); ?>
							<div class="col-md-6">
								<div class="row note-display-wrapper product-display">
									<div class="col-md-12 note-display-category-wrapper">
										<h3 class="note-display-category">
											<?php echo wc_get_product_category_list($product->get_id());?>
										</h3>
									</div>

									<a class="col-md-12 note-display-image-wrapper" href="<?php echo get_site_url() . '/' . $product->get_slug(); ?>">
										<?php if ($discounted_val != $real_val AND $product->is_type('variable') == FALSE): ?>
											<span class="sale-label">Oferta</span>
										<?php endif ?>
										<?php echo woocommerce_get_product_thumbnail('woocommerce_single ');?>
									</a>
									<a class="col-md-12 note-display-title-wrapper" href="<?php echo get_site_url() . '/' . $product->get_slug(); ?>">
										<h1 class="note-display-title"><?php echo get_the_title();?></h1>
										<?php if ($product->is_type('variable') == TRUE): ?>
											<span class="sale-price">
												<?php echo 'Desde ' . wc_price($product->get_price());?>
											</span>
										<?php else: ?>
											<span class="sale-price">
												<?php echo wc_price($product->get_price());?>
											</span>
										<?php endif ?>
										<?php if ($discounted_val != $real_val AND $product->is_type('variable') == FALSE): ?>
											<span class="separator-price">-</span> 
											<span class="discounted-price">
												<?php echo wc_price($product->get_regular_price());?>
											</span>
										<?php endif ?>
									</a>
								</div>
							</div>
						<?php endwhile; ?>
						<?php wp_reset_query(); ?>
					</div>
					<?php // Productos Destacados Module End ?>
					
					<?php // Banner Seccion 2 ?>
					<?php
						$query = new WP_Query( array(
					        'post_type' => 'banner',
					        'category_name' => 'home-seccion-2'
					    ));
					?>
					<?php while ( $query->have_posts() ) : $query->the_post(); ?>
						<div class="row module-separator banner-common">
							<div class="col-md-12 banner-container">
								<?php the_content(); ?>
							</div>
						</div>
					<?php endwhile; ?>
					<?php wp_reset_query(); ?>
					<?php // Banner Seccion 2 End ?>

					<?php // Banner Mobile Seccion 2 ?>
					<?php
						$query = new WP_Query( array(
					        'post_type' => 'bannermobile',
					        'category_name' => 'home-seccion-2'
					    ));
					?>
					<?php while ($query->have_posts()) : $query->the_post();?>
						<div class="row module-separator mobile_banner">
							<div class="col-md-12 banner-container">
								<?php the_content(); ?>
							</div>
						</div>
					<?php endwhile; ?>
					<?php wp_reset_query(); ?>
					<?php // Banner Mobile Seccion 2 End ?>
					
					<?php // Novedades Module ?>
					<div class="row module-separator">
						<div class="col-md-12">
							<h1 class="prod-list-name">Novedades</h1>
						</div>
						<?php  
						    $args = array(
						        'post_type'      => 'product',
						        'posts_per_page' => 6,
						        'orderby' =>'date',
						        'order' => 'DESC'
						    );
						    $loop = new WP_Query( $args );
						?>
						<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
							<?php $real_val = wc_price($product->get_price()); ?>
							<?php $discounted_val = wc_price($product->get_regular_price()); ?>
							<div class="col-lg-4 col-sm-6">
								<div class="row note-display-wrapper product-display">
									<div class="col-md-12 note-display-category-wrapper">
										<h3 class="note-display-category">
											<?php echo wc_get_product_category_list($product->get_id());?>
										</h3>
									</div>
									<a class="col-md-12 note-display-image-wrapper" href="<?php echo get_site_url() . '/' . $product->get_slug(); ?>">
										<?php if ($discounted_val != $real_val AND $product->is_type('variable') == FALSE): ?>
											<span class="sale-label">Oferta</span>
										<?php endif ?>
										<?php echo woocommerce_get_product_thumbnail('woocommerce_single ');?>
									</a>
									<a class="col-md-12 note-display-title-wrapper" href="<?php echo get_site_url() . '/' . $product->get_slug(); ?>">
										<h1 class="note-display-title"><?php echo get_the_title();?></h1>
										<?php if ($product->is_type('variable') == TRUE): ?>
											<span class="sale-price">
												<?php echo 'Desde ' . wc_price($product->get_price());?>
											</span>
										<?php else: ?>
											<span class="sale-price">
												<?php echo wc_price($product->get_price());?>
											</span>
										<?php endif ?>
										<?php if ($discounted_val != $real_val AND $product->is_type('variable') == FALSE): ?>
											<span class="separator-price">-</span> 
											<span class="discounted-price">
												<?php echo wc_price($product->get_regular_price());?>
											</span>
										<?php endif ?>
									</a>
								</div>
							</div>
						<?php endwhile; ?>
						<?php wp_reset_query(); ?>
					</div>
					<?php // Novedades Module End ?>
					
					<?php // Banner Seccion 3 ?>
					<?php
						$query = new WP_Query( array(
					        'post_type' => 'banner',
					        'category_name' => 'home-seccion-3'
					    ));
					?>
					<?php while ( $query->have_posts() ) : $query->the_post(); ?>
						<div class="row module-separator banner-common">
							<div class="col-md-12 banner-container">
								<?php the_content(); ?>
							</div>
						</div>
					<?php endwhile; ?>
					<?php wp_reset_query(); ?>
					<?php // Banner Seccion 3 End ?>

					<?php // Banner Mobile Seccion 3 ?>
					<?php
						$query = new WP_Query( array(
					        'post_type' => 'bannermobile',
					        'category_name' => 'home-seccion-3'
					    ));
					?>
					<?php while ($query->have_posts()) : $query->the_post();?>
						<div class="row module-separator mobile_banner">
							<div class="col-md-12 banner-container">
								<?php the_content(); ?>
							</div>
						</div>
					<?php endwhile; ?>
					<?php wp_reset_query(); ?>
					<?php // Banner Mobile Seccion 3 End ?>
					
					<?php // Ofertas Module ?>
					<div class="row module-separator">
						<div class="col-md-12">
							<h1 class="prod-list-name">Ofertas</h1>
						</div>
						<?php  
						    $args = array(
						        'post_type'      => 'product',
						        'posts_per_page' => 10
						    );
						    $loop = new WP_Query($args);
						?>
						<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
							<?php $real_val = wc_price($product->get_price()); ?>
							<?php $discounted_val = wc_price($product->get_regular_price()); ?>
							<?php if ($discounted_val != $real_val AND $product->is_type('variable') == FALSE): ?>
								<div class="col-lg-4 col-sm-6">
									<div class="row note-display-wrapper product-display">
										<div class="col-md-12 note-display-category-wrapper">
											<h3 class="note-display-category">
												<?php echo wc_get_product_category_list($product->get_id());?>
											</h3>
										</div>
										<a class="col-md-12 note-display-image-wrapper" href="<?php echo get_site_url() . '/' . $product->get_slug(); ?>">
											<?php if ($discounted_val != $real_val AND $product->is_type('variable') == FALSE): ?>
												<span class="sale-label">Oferta</span>
											<?php endif ?>
											<?php echo woocommerce_get_product_thumbnail('woocommerce_single ');?>
										</a>
										<a class="col-md-12 note-display-title-wrapper" href="<?php echo get_site_url() . '/' . $product->get_slug(); ?>">
											<h1 class="note-display-title"><?php echo get_the_title();?></h1>
											<?php if ($product->is_type('variable') == TRUE): ?>
												<span class="sale-price">
													<?php echo 'Desde ' . wc_price($product->get_price());?>
												</span>
											<?php else: ?>
												<span class="sale-price">
													<?php echo wc_price($product->get_price());?>
												</span>
											<?php endif ?>
											<?php if ($discounted_val != $real_val AND $product->is_type('variable') == FALSE): ?>
												<span class="separator-price">-</span> 
												<span class="discounted-price">
													<?php echo wc_price($product->get_regular_price());?>
												</span>
											<?php endif ?>
										</a>
										
									</div>
								</div>
							<?php endif ?>
						<?php endwhile; ?>
						<?php wp_reset_query(); ?>
					</div>
					<?php // Ofertas Module End ?>

				</div>

			</div>
			
		</div>
	</div>
	<?php get_footer('shop'); ?>
</body>
</html>