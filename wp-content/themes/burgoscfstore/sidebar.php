<div class="sidebar-wrapper" id="sidebar" role="complementary">
	<ul class="sidebar-element form-module">
		<li>
			<?php get_search_form(); ?>
		</li>
	</ul>
	<ul class="sidebar-element dashboard-module">
		<li class="pagenav">
			<h2>Panel de Control</h2>
			<ul>
				<li class="page_item page-item-6">
					<a href="<?php echo get_site_url();?>/tienda/">Ver Tienda</a>
				</li>
				<li class="page_item page-item-9">
					<a href="<?php echo get_site_url();?>/mi-cuenta/">Mi cuenta</a>
				</li>
				<li class="page_item page-item-7">
					<a href="<?php echo get_site_url();?>/carro/">Carrito</a>
				</li>
				<li class="page_item page-item-8">
					<a href="<?php echo get_site_url();?>/finalizar-comprar/">Finalizar la compra</a>
				</li>
				<?php if (is_user_logged_in()): ?>
					<li>
						<a href="<?php echo wc_logout_url();?>">
							Cerrar Sesion
						</a>
					</li>
				<?php endif ?>
			</ul>
		</li>		
	</ul>
	<ul class="sidebar-element categories-module" role="navigation">
		<li><h2>Categorias</h2></li>
		<li>
			<?php
				$cat_args = array(
				    'orderby'    => 'name',
				    'order'      => 'asc',
				    'hide_empty' => TRUE,
				);
				$product_categories = get_terms('product_cat', $cat_args);
			?>
			<?php if (!empty($product_categories)): ?>
				<ul>
					<?php foreach ($product_categories as $key => $category): ?>
						<li>
							<a href="<?php echo get_term_link($category);?>">
								<?php echo $category->name;?>
							</a>
						</li>
					<?php endforeach ?>
				</ul>
			<?php endif ?>
		</li>
	</ul>
</div>

