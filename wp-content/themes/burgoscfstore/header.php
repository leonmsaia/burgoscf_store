<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php bloginfo( 'name' ); ?> - <?php echo wp_get_document_title(); ?></title>

	<link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/assets/css/bootstrap.css">
	<link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/assets/css/theme.css">

	<link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/assets/css/font-awesome.css">
	<link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/assets/css/all.css">

	<link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/assets/css/jssocials.css">
	<link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/assets/css/jssocials-theme-plain.css">

	<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,200;0,500;0,900;1,200;1,500;1,900&display=swap" rel="stylesheet">
	
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_bloginfo('template_directory'); ?>/assets/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_bloginfo('template_directory'); ?>/assets/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_bloginfo('template_directory'); ?>/assets/favicon/favicon-16x16.png">
	<link rel="manifest" href="<?php echo get_bloginfo('template_directory'); ?>/assets/favicon/site.webmanifest">
	<link rel="mask-icon" href="<?php echo get_bloginfo('template_directory'); ?>/assets/favicon/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="theme-color" content="#ffffff">

	<script src="<?php echo get_bloginfo('template_directory'); ?>/assets/js/jquery-3.2.1.min.js"></script>
	<script src="<?php echo get_bloginfo('template_directory'); ?>/assets/js/bootstrap.js"></script>

	<?php wp_head(); ?>
</head>
<body>
