<div class="container-fluid main-footer-container">
	<div class="main-footer-design-white"></div>
	<div class="main-footer-design-grass"></div>
</div>
<div class="container-fluid main-footer-container dark-bg">
	<div class="container main-footer-body">
		<div class="row">
			<div class="col-md-4">
				<div class="row footer-module-wrapper">
					<div class="col-md-12">
						<h4 class="footer-module-title">REDES SOCIALES</h4>
					</div>
					<div class="col-md-12 display-social-share-box footer-social">
						<ul class="social-share-list-container">
							<li class="social-share-list-item-container">
								<a href="https://www.facebook.com/burgoscf/" target="_blank"
									>
									<i class="fab fa-facebook-square"></i>
								</a>
							</li>
							<li class="social-share-list-item-container">
								<a href="https://twitter.com/burgos_cf" target="_blank"
									>
									<i class="fab fa-twitter-square"></i>
								</a>
							</li>
							<li class="social-share-list-item-container">
								<a href="https://www.instagram.com/burgos_cf/" target="_blank"
									>
									<i class="fab fa-instagram-square"></i>
								</a>
							</li>
							<li class="social-share-list-item-container">
								<a href="https://www.youtube.com/user/BurgosClubFutbol" target="_blank"
									>
									<i class="fab fa-youtube-square"></i>
								</a>
							</li>
						</ul>
						<p class="footer-module-desc">
							UNETE A NOSOTROS
							<br>
							EN LAS REDES SOCIALES
						</p>
						</div>
					</div>
				</div>
			<div class="col-md-4">
				<div class="footer-module-logo-container">
					<img src="<?php echo get_bloginfo('template_directory'); ?>/assets/logo_w.svg" alt="">
				</div>
			</div>
			<div class="col-md-4">
				<img src="<?php echo get_bloginfo('template_directory'); ?>/assets/img/assets/footer_legnd.png" alt="" style="width: 100%;">
			</div>
		</div>
	</div>
</div>
<div class="container-fluid main-footer-container">
	<div class="container footer-menu-container">
		<ul class="footer-menu-wrapper">
			<li class="footer-menu-element">
				<a href="<?php echo get_site_url();?>">Home</a>
			</li>
			<?php
			$cat_args = array(
			    'orderby'    => 'name',
			    'order'      => 'asc',
			    'hide_empty' => TRUE,
			);
			$product_categories = get_terms('product_cat', $cat_args);
			?>
			<?php if (!empty($product_categories)): ?>
			  <?php foreach ($product_categories as $key => $category): ?>
			    <?php if ($category->parent == 0): ?>
					<li class="footer-menu-element">
						<a href="<?php echo get_term_link($category);?>"><?php echo $category->name;?></a>
					</li>
			    <?php endif ?>
			  <?php endforeach ?>
			<?php endif ?>
		</ul>
	</div>
	<div class="container footer-copy-container">
		<p class="copy-txt">&copy; <?php echo date('Y');?>. desarrollado por burgos c.f. 200. Todos los derechos reservados.</p>
		<p class="legal-txt"><a href="<?php echo get_site_url();?>/aviso-legal/">aviso</a> | <a href="<?php echo get_site_url();?>/politica-de-cookies/">politica de cookies</a> | <a href="<?php echo get_site_url();?>?page_id=3">politica de privacidad</a></p>
	</div>
	<div class="main-footer-design-line"></div>
</div>
<?php wp_footer(); ?>