<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>

<?php get_header('shop'); ?>
<div class="container-fluid top-navbar-wrapper">
	<span class="top-navbar-border-white"></span>
	<span class="top-navbar-border-black"></span>
</div>
<?php get_template_part('sections/modules/user-navbar');?>
<?php get_template_part('sections/modules/header-simplex');?>

<div class="container-fluid main-body-container">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 next-match-boxes-container">
				<div class="row">
				</div>
			</div>
			<div class="col-lg-12">
				<div class="row">
					<div class="col-lg-12 note-display-inside">
						<div class="row note-display-wrapper">
							<div class="col-lg-3 sidebar-container">
								<?php do_action('woocommerce_sidebar');?>
							</div>
							<div class="col-lg-9">
								<?php while ( have_posts() ) : ?>
									<?php the_post(); ?>
									<div class="row product-categories-wrapper">
										<div class="col-lg-12">
											<?php woocommerce_template_single_meta();?>
										</div>
									</div>
									<div class="row product-basic-information-wrapper">
										<div class="col-lg-6 product-image-wrapper">
											<?php woocommerce_show_product_sale_flash();?>
											<?php woocommerce_show_product_images();?>
										</div>
										<div class="col-lg-6 product-stats-wrapper">
											<?php woocommerce_template_single_title();?>
											<?php woocommerce_template_single_rating();?>
											<?php woocommerce_template_single_price();?>
											<?php woocommerce_template_single_excerpt();?>
											<?php woocommerce_template_single_add_to_cart();?>
											<?php woocommerce_template_single_sharing();?>
										</div>
									</div>
									<div class="row product-tabs-wrapper">
										<div class="col-lg-12">
											<?php woocommerce_output_product_data_tabs();?>
										</div>
									</div>
									<div class="row product-upsell-wrapper">
										<div class="col-lg-12">
											<?php woocommerce_upsell_display();?>
										</div>
									</div>
									<div class="row product-related-wrapper">
										<div class="col-lg-12">
											<?php woocommerce_output_related_products();?>
										</div>
									</div>
								<?php endwhile;?>
							</div>
						</div>
					</div>
				</div>				
			</div>
		</div>
	</div>
</div>
<?php get_footer('shop'); ?>
</body>
</html>