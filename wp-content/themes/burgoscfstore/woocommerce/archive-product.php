<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;
?>

<?php get_header('shop'); ?>
<div class="container-fluid top-navbar-wrapper">
	<span class="top-navbar-border-white"></span>
	<span class="top-navbar-border-black"></span>
</div>
<?php get_template_part('sections/modules/user-navbar');?>
<?php get_template_part('sections/modules/header-simplex');?>

<div class="container-fluid main-body-container">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 next-match-boxes-container">
				<div class="row">
				</div>
			</div>
			<div class="col-lg-12">
				<div class="row">
					<div class="col-lg-12 note-display-inside">
						<div class="row note-display-wrapper">
							<div class="col-lg-3 sidebar-container">
								<?php do_action('woocommerce_sidebar');?>
							</div>
							<div class="col-lg-9">
								<?php woocommerce_output_content_wrapper();?>
								<?php woocommerce_breadcrumb();?>

								<?php if (woocommerce_product_loop()): ?>
									
									<?php woocommerce_output_all_notices();?>
									<?php woocommerce_result_count();?>
									<?php woocommerce_catalog_ordering();?>
									
									<div class="related products">
										<?php woocommerce_product_loop_start();?>
											
										<?php if (wc_get_loop_prop('total')): ?>
											<?php while (have_posts()) {?>
												<?php the_post();?>
												<?php do_action( 'woocommerce_shop_loop' );?>
												<?php wc_get_template_part( 'content', 'product' );?>
											<?php }?>
										<?php endif ?>

										<?php woocommerce_product_loop_end();?>
									</div>
									<?php woocommerce_pagination();?>

								<?php else: ?>
									
									<?php wc_no_products_found();?>

								<?php endif ?>

								<?php woocommerce_output_content_wrapper_end();?>
							</div>
						</div>
					</div>
				</div>				
			</div>
		</div>
	</div>
</div>
<?php get_footer('shop'); ?>
</body>
</html>





