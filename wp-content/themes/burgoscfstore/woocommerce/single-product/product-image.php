<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.1
 */

defined( 'ABSPATH' ) || exit;

// Note: `wc_get_gallery_image_html` was added in WC 3.3.2 and did not exist prior. This check protects against theme overrides being used on older versions of WC.
if ( ! function_exists( 'wc_get_gallery_image_html' ) ) {
	return;
}

global $product;

$columns           = apply_filters( 'woocommerce_product_thumbnails_columns', 4 );
$post_thumbnail_id = $product->get_image_id();
$wrapper_classes   = apply_filters(
	'woocommerce_single_product_image_gallery_classes',
	array(
		'woocommerce-product-gallery',
		'woocommerce-product-gallery--' . ( $product->get_image_id() ? 'with-images' : 'without-images' ),
		'woocommerce-product-gallery--columns-' . absint( $columns ),
		'images',
	)
);
?>
<?php
	$have_thumbs = count($product->get_gallery_image_ids());
	$main_picture = wp_get_attachment_image_src($post_thumbnail_id,'full')[0];
	$attachment_ids = $product->get_gallery_image_ids();
?>
<?php if ($have_thumbs > 0): ?>
<div class="prod-image-wrapper">
	<div id="carouselExampleControls" class="carousel slide" data-ride="carousel" data-interval="false">
		<div class="carousel-inner">
			<div class="carousel-item active">
				<img class="img-fluid" src="<?php echo $main_picture;?>" alt="" data-toggle="modal" data-target="#slide_0">
				<div class="modal fade" id="slide_0" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<img src="<?php echo $main_picture;?>" class="img-fluid" alt="">
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php $counter_flag = 1;?>
			<?php foreach ($attachment_ids as $attachment_id): ?>
				<div class="carousel-item">
					<img class="img-fluid" src="<?php echo wp_get_attachment_image_src($attachment_id,'full')[0];?>" alt="" data-toggle="modal" data-target="#slide_<?php echo $counter_flag;?>">
					<div class="modal fade" id="slide_<?php echo $counter_flag;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<img src="<?php echo wp_get_attachment_image_src($attachment_id,'full')[0];?>" class="img-fluid" alt="">
								</div>
							</div>
						</div>
					</div>
				</div>		
				<?php $counter_flag++;?>			
			<?php endforeach ?>
		</div>
		<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>
</div>
<?php else: ?>
<div class="prod-image-wrapper">
	<img class="img-fluid" src="<?php echo $main_picture;?>" alt="" data-toggle="modal" data-target="#slide_0">
	<div class="modal fade" id="slide_0" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<img src="<?php echo $main_picture;?>" class="img-fluid" alt="">
				</div>
			</div>
		</div>
	</div>
</div>
<?php endif ?>