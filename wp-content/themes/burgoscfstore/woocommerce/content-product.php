<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;
?>

<?php global $product;?>
<?php $real_val = wc_price($product->get_price()); ?>
<?php $discounted_val = wc_price($product->get_regular_price()); ?>
<li class="product-listing-item col-lg-12">
	<div class="row">
		<div class="col-lg-12 note-display-category-wrapper">
			<span class="rating_module"><?php woocommerce_template_loop_rating();?></span>
			<h3 class="note-display-category">
				<?php echo wc_get_product_category_list($product->get_id());?>
			</h3>
		</div>
		<a class="col-lg-12 note-display-image-wrapper" href="<?php echo get_site_url() . '/' . $product->get_slug(); ?>">
			<?php if ($discounted_val != $real_val AND $product->is_type('variable') == FALSE): ?>
				<span class="sale-label">Oferta</span>
			<?php endif ?>
			<?php echo woocommerce_get_product_thumbnail('woocommerce_single ');?>
		</a>
		<a class="col-lg-12 note-display-title-wrapper" href="<?php echo get_site_url() . '/' . $product->get_slug(); ?>">
			<h1 class="note-display-title"><?php echo get_the_title();?></h1>
		</a>
		<div class="col-lg-12 note-display-price-wrapper">
			<div class="row">
				<div class="col-lg-12">
					<?php if ($product->is_type('variable') == TRUE): ?>
						<span class="sale-price">
							<?php echo 'Desde ' . wc_price($product->get_price());?>
						</span>
					<?php else: ?>
						<span class="sale-price">
							<?php echo wc_price($product->get_price());?>
						</span>
					<?php endif ?>
					<?php if ($discounted_val != $real_val AND $product->is_type('variable') == FALSE): ?>
						<span class="separator-price">-</span> 
						<span class="discounted-price">
							<?php echo wc_price($product->get_regular_price());?>
						</span>
					<?php endif ?>
				</div>
				<div class="col-lg-12 add_to_cart_wrapper">
					<?php woocommerce_template_loop_add_to_cart();?>		
				</div>
			</div>
		</div>
	</div>
</li>