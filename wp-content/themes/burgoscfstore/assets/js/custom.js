document.addEventListener('DOMContentLoaded', function() {
  var calendarEl = document.getElementById('calendar');
  var calendar = new FullCalendar.Calendar(calendarEl, {
    locale: 'es',
    plugins: ['interaction', 'dayGrid'],
    editable: false,
    eventLimit: false
  });
  $.ajax({
    type: 'GET', 
    url: 'assets/matches.json',
    dataType: 'json',
    success: function(respuesta) {
      for (var i = respuesta.length - 1; i >= 0; i--) {
        console.log(respuesta[i]);
        calendar.addEvent({
          title: respuesta[i].title,
          start: respuesta[i].start,
          end: respuesta[i].end,
          url: respuesta[i].url
        });
      }
    }

  });
  calendar.render();
});
eventClick:  function(event, jsEvent, view) {
    $('#modalTitle').html(event.title);
    $('#modalBody').html(event.description);
    $('#eventUrl').attr('href',event.url);
    $('#calendarModal').modal();
},
