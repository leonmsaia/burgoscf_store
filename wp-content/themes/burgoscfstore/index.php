<!DOCTYPE html>
<html>
<head>
	<?php get_template_part('sections/assets/head');?>
</head>
<body>
	<div class="container-fluid top-navbar-wrapper">
		<span class="top-navbar-border-white"></span>
		<span class="top-navbar-border-black"></span>
	</div>
	<?php get_template_part('sections/modules/user-navbar');?>
	<?php get_template_part('sections/modules/header-complex');?>
	<?php get_template_part('sections/modules/hero-slider');?>
	<div class="container-fluid main-body-container home-wrapper">
		<div class="container">
			<div class="row">
				
				<div class="col-md-12">
					
					<div class="row module-separator banner-common">
						<div class="col-md-12 banner-container">
							<a href="<?php echo get_site_url();?>/categoria-producto/juego/primera-equipacion/">
								<img src="<?php echo get_bloginfo('template_directory'); ?>/assets/img/banners/banner_local.png" alt="" class="img-fluid">
							</a>
						</div>
					</div>

					<div class="row module-separator banner-common">
						<div class="col-md-12 banner-container">
							<a href="<?php echo get_site_url();?>/categoria-producto/juego/segunda-equipacion/">
								<img src="<?php echo get_bloginfo('template_directory'); ?>/assets/img/banners/banner_visitante.png" alt="" class="img-fluid">
							</a>
						</div>
					</div>

					<div class="row module-separator banner-common">
						<div class="col-md-12 banner-container">
							<a href="<?php echo get_site_url();?>/categoria-producto/juego/tercera-equipacion/">
								<img src="<?php echo get_bloginfo('template_directory'); ?>/assets/img/banners/banner_tercera.png" alt="" class="img-fluid">
							</a>
						</div>
					</div>

					<div class="row module-separator banner-common">
						<div class="col-md-12 banner-container">
							<a href="<?php echo get_site_url();?>/categoria-producto/entrenamiento/">
								<img src="<?php echo get_bloginfo('template_directory'); ?>/assets/img/banners/banner_entrenamiento.png" alt="" class="img-fluid">
							</a>
						</div>
					</div>

					<div class="row module-separator banner-common">
						<div class="col-md-12 banner-container">
							<a href="<?php echo get_site_url();?>/categoria-producto/complementos/">
								<img src="<?php echo get_bloginfo('template_directory'); ?>/assets/img/banners/banner_paseo.png" alt="" class="img-fluid">
							</a>
						</div>
					</div>

				</div>

			</div>
			
		</div>
	</div>
	<?php get_footer('shop'); ?>
</body>
</html>