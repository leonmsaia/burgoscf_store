<?php get_header('shop'); ?>
<div class="container-fluid top-navbar-wrapper">
	<span class="top-navbar-border-white"></span>
	<span class="top-navbar-border-black"></span>
</div>
<?php get_template_part('sections/modules/user-navbar');?>
<?php get_template_part('sections/modules/header-simplex');?>

<div class="container-fluid main-body-container">
	<div class="container">
		<div class="row">
			<div class="col-md-12 next-match-boxes-container">
				<div class="row">
				</div>
			</div>
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-12 note-display-inside">
						<div class="row note-display-wrapper">
							<div class="col-md-3 sidebar-container">
								<?php do_action('woocommerce_sidebar');?>
							</div>
							<div class="col-md-9">
								<div class="row">
									<?php while (have_posts()) : ?>
										<?php the_post(); ?>
										<div class="col-md-12 module-date-stat">
											<?php //echo get_the_date(); ?>
										</div>
										<div class="col-md-12 module-headline">
											<h2><?php the_title(); ?></h2>
										</div>
										<div class="col-md-12 module-content">
											<?php the_content(); ?>
										</div>
									<?php endwhile; ?>
								</div>
							</div>
						</div>
					</div>
				</div>				
			</div>
		</div>
	</div>
</div>
<?php get_footer('shop'); ?>
</body>
</html>