<div class="container-fluid main-footer-container">
	<div class="main-footer-design-white"></div>
	<div class="main-footer-design-grass"></div>
</div>
<div class="container-fluid main-footer-container dark-bg">
	<div class="container main-footer-body">
		<div class="row">
			<div class="col-md-4">
				<div class="row footer-module-wrapper">
					<div class="col-md-12">
						<h4 class="footer-module-title">REDES SOCIALES</h4>
					</div>
					<div class="col-md-12">
						<p class="footer-module-desc">UNITE A NOSOTROS<br>EN LAS REDES SOCIALES</p>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="footer-module-logo-container">
					<img src="<?php echo get_bloginfo('template_directory'); ?>/assets/logo_w.svg" alt="">
				</div>
			</div>
			<div class="col-md-4">
				<div class="row footer-module-wrapper">
					<div class="col-md-12">
						<h4 class="footer-module-title">NEWSLETTERS</h4>
					</div>
					<div class="col-md-12">
						<p class="footer-module-desc">ENTERATE DE<br>LAS ULTIMAS NOTICIAS</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid main-footer-container">
	<div class="container footer-menu-container">
		<ul class="footer-menu-wrapper">
			<li class="footer-menu-element">
				<a href="#">Home</a>
			</li>
			<li class="footer-menu-element">
				<a href="#">Actualidad</a>
			</li>
			<li class="footer-menu-element">
				<a href="#">El Club</a>
			</li>
			<li class="footer-menu-element">
				<a href="#">Primer Equipo</a>
			</li>
			<li class="footer-menu-element">
				<a href="#">Prensa</a>
			</li>
			<li class="footer-menu-element">
				<a href="#">Juegos</a>
			</li>
			<li class="footer-menu-element">
				<a href="#">Patrocinadores</a>
			</li>
			<li class="footer-menu-element">
				<a href="#">Cantera</a>
			</li>
		</ul>
	</div>
	<div class="container footer-copy-container">
		<p class="copy-txt">desarrollado por burgos c.f. 200. Todos los derechos reservados.</p>
		<p class="legal-txt"><a href="#">aviso</a> | <a href="#">politica de cookies</a> | <a href="#">politica de privacidad</a></p>
	</div>
	<div class="main-footer-design-line"></div>
</div>