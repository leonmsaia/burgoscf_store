<?php
	$query = new WP_Query( array(
        'post_type' => 'banner',
        'category_name' => 'banner-carousel-home'
    ));
?>
<div id="BlanquiHeroSlide" class="container-fluid carousel slide hero-image-container" data-ride="carousel" data-interval="6000">
	
	<div class="row hero-slide-container carousel-inner" role="listbox">
		<?php // Hero Slide Item ?>
		<?php
			$counter = 0;
		?>
		<?php while ( $query->have_posts() ) : $query->the_post(); ?>
			<?php
				$link_builder = get_the_excerpt();
				if ($counter == 0) {
					$flag = 'active';
				}else{
					$flag = '';
				}
			?>
			<div class="col-md-12 hero-main-note-container carousel-item <?php echo $flag;?>" style="background-image: url('<?php echo get_the_post_thumbnail_url();?>'); ">
				<div class="container main-message-hero">
					<div class="col-md-8">
						<div class="row">
							<div class="col-md-10 hero-singular-note-container">
								<h1 class="hero-slide-title">
									<?php the_title(); ?>
								</h1>
								<a href="<?php echo $link_builder;?>" class="readmore-button">Ver Más</a>
							</div>
							<div class="col-md-2"></div>
						</div>
					</div>
				</div>
			</div>

			<?php $counter++; ?>
		<?php endwhile; ?>
		<?php wp_reset_query(); ?>
		<?php // Hero Slide Item End ?>
	</div>

	<a class="carousel-control-prev" href="#BlanquiHeroSlide" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only"></span>
    </a>
    <a class="carousel-control-next" href="#BlanquiHeroSlide" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only"></span>
    </a>

	<ol class="carousel-indicators hero-sidenotes-container container">
		<?php // Hero Indicator ?>
		<?php
			$counter_indicator = 0;
		?>
		<?php while ( $query->have_posts() ) : $query->the_post(); ?>
			<?php
				if ($counter_indicator == 0) {
					$flag = 'active';
				}else{
					$flag = '';
				}
			?>
			<li data-target="#BlanquiHeroSlide" data-slide-to="<?php echo $counter_indicator;?>" class="hero-note-sidenote-singular <?php echo $flag;?>">
				<h3><?php the_title(); ?></h3>
			</li>
			<?php $counter_indicator++; ?>
		<?php endwhile; ?>
		<?php wp_reset_query(); ?>
		<?php // Hero Indicator End ?>
	</ol>

</div>