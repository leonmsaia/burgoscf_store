<div class="row">
	<div class="col-md-12 hero-slide-container">
		<div class="row hero-image-container">
			<div class="col-md-12 background-container">
				<span class="black-shade-container"></span>
				<img src="<?php echo get_bloginfo('template_directory'); ?>/assets/img/pics/Burgos-Salamanca (124).jpg" alt="">
			</div>
		</div>
		<div class="row hero-main-note-container">
			<div class="col-md-8">
				<div class="row">
					<div class="col-md-10 hero-singular-note-container">
						<h4 class="hero-slide-category">Burgos CF</h4>
						<h1 class="hero-slide-title">El Burgos Club de Futbol presenta un Erte</h1>
						<a href="#" class="readmore-button">Leer</a>
					</div>
					<div class="col-md-2"></div>
				</div>
			</div>
			<div class="col-md-4">
				<ul class="hero-sidenotes-container">
					<?php // Banner Seccion 1 ?>
					<?php
						$query = new WP_Query( array(
					        'post_type' => 'banner',
					        'category_name' => 'banner-carousel-home'
					    ));
					?>
					<?php while ( $query->have_posts() ) : $query->the_post(); ?>
						<li class="hero-note-sidenote-singular">
							<h3><?php the_title(); ?></h3>
							<span class="date"><?php echo get_the_date('l j, F Y');?></span>
							<?php echo get_the_excerpt();?>
							<?php echo get_the_post_thumbnail_url();?>
						</li>
						
					<?php endwhile; ?>
					<?php wp_reset_query(); ?>
					<?php // Banner Seccion 1 End ?>

				</ul>
			</div>
		</div>
	</div>		
</div>

