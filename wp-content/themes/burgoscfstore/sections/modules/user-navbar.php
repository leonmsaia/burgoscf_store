<div class="container-fluid user-navbar-wrapper">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="user-navbar-toolbox">
					<?php
						// Cart System Tools
						global $woocommerce;
						$total_items = WC()->cart->get_cart_contents_count();
						$total_cart = WC()->cart->get_cart_total();
					?>
					<?php if (is_user_logged_in()): ?>
						<?php
							global $current_user;
							wp_get_current_user();
						?>
						<li>
							<a href="<?php echo get_site_url() . '/mi-cuenta'?>">
								Bienvenido/a <?php echo $current_user->user_firstname;?>!
							</a>
						</li>
					<?php else: ?>
						<li>
							<a href="<?php echo get_site_url() . '/mi-cuenta'?>">Iniciar Sesión</a>
							 o 
							<a href="<?php echo get_site_url() . '/registrarme'?>">Registrarme</a>
						</li>
					<?php endif ?>
					<li>
						 <a href="<?php echo wc_get_cart_url(); ?>">
						 	<i class="fas fa-shopping-cart"></i> 
						 	[<?php echo $total_cart;?> (<?php echo $total_items;?> items)]	
					 	</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>