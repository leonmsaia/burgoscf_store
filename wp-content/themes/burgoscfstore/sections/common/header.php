<div class="row header-container">
	<div class="col-lg-12 regular-header-wrapper">
		<div class="row">
			<div class="col-lg-2">
				<a href="<?php echo get_site_url();?>">
					<img class="logo-holder" src="<?php echo get_bloginfo('template_directory'); ?>/assets/logo_w.svg" alt="">
				</a>
			</div>
			<div class="col-lg-10">
				<?php get_template_part('sections/common/navbar');?>
			</div>
		</div>
	</div>
	<?php get_template_part('sections/common/navbar_mobile');?>
</div>