<div class="col-md-12 mobile-header-wrapper">
  <div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4">
      <a href="<?php echo get_site_url();?>">
        <img class="bn_logo" src="<?php echo get_bloginfo('template_directory'); ?>/assets/logo_bn.svg" alt="">
        <img class="w_logo" src="<?php echo get_bloginfo('template_directory'); ?>/assets/logo_w.svg" alt="">
      </a>
    </div>
    <div class="col-md-4"></div>
  </div>
  <div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6">
      <nav class="navbar navbar-light">
        <button class="navbar-toggler toggler-example" type="button" data-toggle="collapse" data-target="#navbarSupportedContent1"
        aria-controls="navbarSupportedContent1" aria-expanded="false" aria-label="Toggle navigation">
          <span class="dark-blue-text">
              <i class="fas fa-bars fa-1x"></i>
          </span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent1">
          <?php
            $cat_args = array(
                'orderby'    => 'name',
                'order'      => 'asc',
                'hide_empty' => TRUE,
            );
            $product_categories = get_terms('product_cat', $cat_args);
          ?>
          <ul class="navbar-nav">  
            <?php if (!empty($product_categories)): ?>
              <?php foreach ($product_categories as $key => $category): ?>
                <?php if ($category->parent == 0): ?>
                  <li class="nav-item dropdown">
                      <a class="nav-link dropdown-toggle" href="#" id="parent<?php echo $category->term_id;?>_dropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <?php echo $category->name;?>
                      </a>
                      <?php
                        $first_item_element = '<a class="dropdown-item" href="' . get_term_link($category) . '">' . $category->name . '</a>';
                        $cat_id = $category->term_id;
                        $subcat_args     = array(
                            'parent' => $cat_id,
                            'orderby'    => 'name',
                            'order'      => 'asc',
                            'hide_empty' => TRUE
                        );
                        $product_subcategories = get_terms('product_cat', $subcat_args);
                      ?>
                      <div class="dropdown-menu" aria-labelledby="parent_<?php echo $category->term_id;?>_dropdown">
                      <?php echo $first_item_element;?>
                      <?php foreach ($product_subcategories as $key => $subcategory): ?>
                        <a class="dropdown-item" href="<?php echo get_term_link($subcategory);?>"><?php echo $subcategory->name;?></a>
                      <?php endforeach ?>
                      </div>
                  </li>
                <?php endif ?>
              <?php endforeach ?>
              <?php endif ?>
          </ul>
        </div>
      </nav>
    </div>
    <div class="col-md-3"></div>
  </div>
</div>