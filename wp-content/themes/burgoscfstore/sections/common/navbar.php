<nav class="navbar navbar-expand-lg">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="<?php echo get_site_url();?>">Home</a>
      </li>
      <?php
        $cat_args = array(
            'orderby'    => 'name',
            'order'      => 'asc',
            'hide_empty' => TRUE,
        );
        $product_categories = get_terms('product_cat', $cat_args);
      ?>
      <?php if (!empty($product_categories)): ?>
          <?php foreach ($product_categories as $key => $category): ?>
            <?php if ($category->parent == 0): ?>
              <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="parent<?php echo $category->term_id;?>_dropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <?php echo $category->name;?>
                  </a>
                  <?php
                    $first_item_element = '<a class="dropdown-item" href="' . get_term_link($category) . '">' . 'Ver todos los Productos' . '</a>';
                    $cat_id = $category->term_id;
                    $subcat_args     = array(
                        'parent' => $cat_id,
                        'orderby'    => 'name',
                        'order'      => 'asc',
                        'hide_empty' => TRUE
                    );
                    $product_subcategories = get_terms('product_cat', $subcat_args);
                  ?>
                  <div class="dropdown-menu" aria-labelledby="parent_<?php echo $category->term_id;?>_dropdown">
                  <?php foreach ($product_subcategories as $key => $subcategory): ?>
                    <a class="dropdown-item" href="<?php echo get_term_link($subcategory);?>">
                      <?php echo $subcategory->name;?>    
                    </a>
                  <?php endforeach ?>
                  <?php echo $first_item_element;?>
                  </div>
              </li>
            <?php endif ?>
          <?php endforeach ?>
      <?php endif ?>
    </ul>
  </div>
</nav>